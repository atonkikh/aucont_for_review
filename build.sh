#!/bin/sh -eux

(cd src && cargo build)
mkdir -p bin
for file in $(find | grep -P 'src/target/debug/aucont_[a-z_]+$'); do
  cp "$file" bin/
done
